//
//  NippouAppDelegate.h
//  Nippou6
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NippouDailyInfoEntity.h"
#import "NippouCoreData.h"
#import <CoreData/CoreData.h>

@interface NippouAppDelegate : UIResponder <UIApplicationDelegate>{
    
}


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (strong, nonatomic) UIWindow *window;

@property(strong,nonatomic)UINavigationController *navigationController;

/* Core Dataで必要なプロパティ */
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// ここに受け渡ししたい変数を宣言
@property (nonatomic,retain) NSString *strMessage;

@property (nonatomic,retain) NippouDailyInfoEntity *todayInfo;

@end
