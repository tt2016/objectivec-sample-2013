//
//  NippouDetailViewController.h
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NippouDetailViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, UITextFieldDelegate, UINavigationBarDelegate, UINavigationControllerDelegate>{

    //作業一覧
    NSMutableArray *taskList;
    NSMutableArray *timeList;
    NSInteger addCount;
    
    //作業時間
//    NSMutableArray *timeList;
    
    UIActionSheet *pickerViewPopup;
    UIPickerView *timePickerView;
    UIToolbar *pickerToolbar;
    IBOutlet UITextField *textField;
    
    //    NSMutableArray* _items;
    IBOutlet UITableView *taskTableView;

    IBOutlet UIButton *sendButton;
    
    //下段のバー
    IBOutlet UIView *bottomBar;

    //選択したタスク名
    int selectedTaskIndex;
    
    //工程名
    NSString *phaseName;

    int phaseIndex;
    
}


//ピッカービュー用
- (void)showPicker;

@property (copy, nonatomic) NSString *phaseName;
@property int phaseIndex;

@property (strong, nonatomic) id textField;

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property(retain,nonatomic)IBOutlet UILabel *label;

@property(nonatomic,retain)UITableView *taskTableView;

@property (nonatomic, retain) UIPickerView *timePickerView;

//ステッパーのアウトレット
@property(retain,nonatomic)IBOutlet UIStepper *stepper;

- (IBAction)moveToNippouMain:(id)sender;

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;

@property (nonatomic, assign) NSInteger addCount;

@property (nonatomic, retain) NSMutableArray *timeList;
@property (nonatomic, retain) NSMutableArray *taskList;


- (void)addRow:(id)sender;


@end
