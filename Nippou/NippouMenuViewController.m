//
//  NippouMenuViewController.m
//  Nippou
//
//  Created by tt2016 on 2012/10/06.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouMenuViewController.h"
#import <QuartzCore/CALayer.h>

@interface NippouMenuViewController ()

@end

@implementation NippouMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    //日報作成ボタンの作成
    UIButton *btnSample = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSample.frame = CGRectMake(20.0f, 180.0f, 280.0f, 45.0f);
    [btnSample setTitle:@"日報作成" forState:UIControlStateNormal];
    [btnSample setBackgroundColor:    [UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]];
    [btnSample.layer setCornerRadius:10.0];
    [btnSample.layer setBorderColor:
    [UIColor colorWithRed:45/255.0 green:45/255.0 blue:45/255.0 alpha:1].CGColor];

//    [btnSample.layer setBorderWidth:2.0];
    [btnSample setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // ボタン押下時のタイトルの色を変更する
    [btnSample setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    //ボタンの影
    btnSample.layer.shadowOffset = CGSizeMake(2, 2);
    btnSample.layer.shadowOpacity = 0.5f;
    btnSample.layer.shadowColor = [UIColor blackColor].CGColor;

    [btnSample addTarget:self action:@selector(moveToMaster) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnSample];

    
    //TODOボタンの作成
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton.frame = CGRectMake(20.0f, 250.0f, 280.0f, 45.0f);
    [settingButton setTitle:@"経費精算" forState:UIControlStateNormal];
    [settingButton setBackgroundColor:    [UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]];
    [settingButton.layer setCornerRadius:10.0];
    [settingButton.layer setBorderColor:
     [UIColor colorWithRed:45/255.0 green:45/255.0 blue:45/255.0 alpha:1].CGColor];
    
    //    [btnSample.layer setBorderWidth:2.0];
    [settingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // ボタン押下時のタイトルの色を変更する
    [settingButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];

    //ボタンの影
    settingButton.layer.shadowOffset = CGSizeMake(2, 2);
    settingButton.layer.shadowOpacity = 0.5f;
    settingButton.layer.shadowColor = [UIColor blackColor].CGColor;

    [self.view addSubview:settingButton];

    //交通費精算ボタンの作成
    UIButton *settingButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton2.frame = CGRectMake(20.0f, 320.0f, 280.0f, 45.0f);
    [settingButton2 setTitle:@"案件一覧" forState:UIControlStateNormal];
    [settingButton2 setBackgroundColor:    [UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]];
    [settingButton2.layer setCornerRadius:10.0];
    [settingButton2.layer setBorderColor:
     [UIColor colorWithRed:45/255.0 green:45/255.0 blue:45/255.0 alpha:1].CGColor];
    
    //    [btnSample.layer setBorderWidth:2.0];
    [settingButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // ボタン押下時のタイトルの色を変更する
    [settingButton2 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    //ボタンの影
    settingButton2.layer.shadowOffset = CGSizeMake(2, 2);
    settingButton2.layer.shadowOpacity = 0.5f;
    settingButton2.layer.shadowColor = [UIColor blackColor].CGColor;
    
    [self.view addSubview:settingButton2];
    
    //交通費精算ボタンの作成
    UIButton *settingButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
    settingButton3.frame = CGRectMake(20.0f, 390.0f, 280.0f, 45.0f);
    [settingButton3 setTitle:@"設定" forState:UIControlStateNormal];
    [settingButton3 setBackgroundColor:    [UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]];
    [settingButton3.layer setCornerRadius:10.0];
    [settingButton2.layer setBorderColor:
     [UIColor colorWithRed:45/255.0 green:45/255.0 blue:45/255.0 alpha:1].CGColor];
    
    //    [btnSample.layer setBorderWidth:2.0];
    [settingButton3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    // ボタン押下時のタイトルの色を変更する
    [settingButton3 setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    //ボタンの影
    settingButton3.layer.shadowOffset = CGSizeMake(2, 2);
    settingButton3.layer.shadowOpacity = 0.5f;
    settingButton3.layer.shadowColor = [UIColor blackColor].CGColor;
    
    [self.view addSubview:settingButton3];
    
    //ボタン背景
    //    self.btnBackLabel.layer.borderColor = [UIColor whiteColor].CGColor;  //ボーダー色（白）
    self.btnBackLabel.layer.borderWidth = 2.0;  //ボーダー幅（２ピクセル）
    self.btnBackLabel.layer.cornerRadius = 10.0;  //角丸半径（10ピクセル）
    
    //タイマーのセット（一秒）
    NSDate *today = [NSDate date]; //現在時刻を取得
    
    NSCalendar *calender = [NSCalendar currentCalendar]; //現在時刻の時分秒を取得
    unsigned flags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *todayComponents = [calender components:flags fromDate:today];
    int hour = [todayComponents hour];
    int min = [todayComponents minute];
    int sec = [todayComponents second];
    NSString *dateStr;
    
    // NSDateFormatter を用意します。
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    // 変換用の書式を設定します。
    [formatter setDateFormat:@"YYYY/MM/dd"];
    
    // NSDate を NSString に変換します。
    dateStr = [formatter stringFromDate:today];
    
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,min,sec]; //時間を表示
    self.dateLabel.text = dateStr;
    [NSTimer scheduledTimerWithTimeInterval:1.0 //タイマーを発生させる間隔（1秒毎）
                                     target:self //メソッドがあるオブジェクト
                                   selector:@selector(driveClock:) //呼び出すメソッド
                                   userInfo:nil //メソッドに渡すパラメータ 
                                    repeats:YES]; //繰り返し    
}


/*
 *　工程選択画面に遷移する
 */
-(void)moveToMaster{
    
    [self performSegueWithIdentifier:@"moveToMain" sender:self];
    
}

- (void)driveClock:(NSTimer *)timer
{
    NSDate *today = [NSDate date]; //現在時刻を取得
    
    NSCalendar *calender = [NSCalendar currentCalendar]; //現在時刻の時分秒を取得
    unsigned flags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *todayComponents = [calender components:flags fromDate:today];
    int hour = [todayComponents hour];
    int min = [todayComponents minute];
    int sec = [todayComponents second];
    
    self.timeLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",hour,min,sec]; //時間を表示
}




@end
