//
//  NippouTaskInfoEntity.h
//  Nippou
//
//  Created by tt2016 on 2012/10/29.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NippouTaskInfoEntity : NSObject{
    
    //タスク
    NSString *taskName;
    
    //作業時間
    NSNumber *workTime;
    
    //メモ
    NSString *memo;
    
}

@property (nonatomic,retain) NSString *taskName;

@property NSNumber *workTime;

@property (nonatomic,retain) NSString *memo;

@end
