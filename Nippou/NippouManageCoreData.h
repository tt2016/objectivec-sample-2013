//
//  NippouManageCoreData.h
//  Nippou
//
//  Created by tt2016 on 2013/01/31.
//  Copyright (c) 2013年 tt2016. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NippouCoreData.h"

@interface NippouManageCoreData : NippouCoreData {
    
}

- (NSMutableArray*)all:(NSString*)entityName;
- (NSMutableArray*)fetch:(NSString*)entityName limit:(int)limit;
- (NSManagedObject*)entityForInsert:(NSString*)entityname;

@end