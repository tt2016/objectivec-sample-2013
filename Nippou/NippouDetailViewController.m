//
//  NippouDetailViewController.m
//  Nippou6
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouDetailViewController.h"
#import "NippouMainViewController.h"
#import "NippouTaskInfoEntity.h"
#import "NippouAppDelegate.h"
#import "NippouTaskInfoEntity.h"
#import "NippouPhaseInfoEntity.h"
#import "NippouInsertTaskViewController.h"
#import "NippouUtil.h"

#import <QuartzCore/QuartzCore.h>

@interface NippouDetailViewController (){
    
}
@end

@implementation NippouDetailViewController
@synthesize timeList;
@synthesize taskList;
@synthesize addCount;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    [timeList replaceObjectAtIndex:2 withObject:@"3h" ];
    
    
    /* 編集終了ボタン */
    //ボタンの背景色変更
    [[sendButton layer] setCornerRadius:8.0f];
    [[sendButton layer] setMasksToBounds:YES];
    [[sendButton layer] setBorderWidth:1.0];
    [[sendButton layer] setBackgroundColor:
    [[UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]CGColor]];
    [sendButton.layer setBorderColor:
     [[UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1]CGColor]];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    //影
    sendButton.layer.shadowOffset = CGSizeMake(2, 2);
    sendButton.layer.shadowOpacity = 0.5f;
    sendButton.layer.shadowColor = [UIColor blackColor].CGColor;
    //グラデーション
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    gradient2.frame = sendButton.bounds;
    gradient2.colors = [NSArray arrayWithObjects:
                        (id)[UIColor colorWithRed:166/255.0 green:101/255.0 blue:121/255.0 alpha:1].CGColor
                        , (id)[UIColor colorWithRed:56/255.0 green:0/255.0 blue:30/255.0 alpha:1].CGColor
                        , nil];
    [sendButton.layer insertSublayer:gradient2 atIndex:0];

    //背景色をグラデーションに設定
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = bottomBar.bounds;

    gradient.colors =     [NSArray arrayWithObjects:
                           (id)[UIColor colorWithRed:133/255.0 green:58/255.0 blue:61/255.0 alpha:1].CGColor,
                           (id)[UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1].CGColor,nil];

    [bottomBar.layer insertSublayer:gradient atIndex:0];
    
}


/*
 * 画面表示時の処理
 */
- (void)viewWillAppear:(BOOL)animated {
    
    
    //タイトルの変更
    self.title = self.phaseName;
 
	
    //前画面で選択された工程のリストを表示する
/*
    
    taskList = [[NSMutableArray alloc] initWithObjects:@"調査", @"設計書作成", @"内部レビュー", @"客先レビュー", @"レビュー結果反映", nil];
    
    timeList = [[NSMutableArray alloc] initWithObjects:@"", @"3時間", @"2時間", @"", @"45分", nil];

    */

    //デリゲートから日報情報を取得
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    //リストから全件取得する処理が必要
    taskList = [[NSMutableArray alloc] init];
    timeList = [[NSMutableArray alloc] init];
        
    for(NippouPhaseInfoEntity *phase in appDelegate.todayInfo.phaseInfoList){
        
        //前画面から渡された値と一致すればタスク名を表示
        if ( self.phaseName == phase.phaseName){
            
            for(NippouTaskInfoEntity *task in phase.taskInfoList){
                [taskList addObject:task.taskName];
                [timeList addObject:[NippouUtil timeValueToString:task.workTime]];

            }
        }
    }
        
    //セルの選択解除
    [self.taskTableView deselectRowAtIndexPath:[self.taskTableView indexPathForSelectedRow] animated:NO];

    [self.taskTableView reloadData];
        
    [super viewWillAppear:animated];
}


/*
 * タスク追加画面遷移時の前処理
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ( [[segue identifier] isEqualToString:@"moveToInsert"] ) {
        NippouInsertTaskViewController *insertViewController = [segue destinationViewController];
        //選択された工程の番号を次画面に渡す
        
        insertViewController.phaseIndex = self.phaseIndex;
        insertViewController.phaseName = self.phaseName;
    }
}


- (void)addRow:(id)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:taskList.count inSection:0];
    NSArray *indexPaths = [NSArray arrayWithObjects:indexPath,nil];
    [taskList addObject:[NSString stringWithFormat:@"追加された行その%d",addCount]];
    [timeList addObject:[NSString stringWithFormat:@"追加された行その%d",addCount]];
    addCount++; // 次に使うとき用にaddCountに1足しています。
    [taskTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
}


- (void)insertNewObject:(id)sender
{
/*    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.textLabel.text = [taskList objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [self.timeList objectAtIndex:indexPath.row];
  */
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


//テーブル関連のメソッド
#pragma mark - Table View

/**
 * セクション数
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //タスク
    //タスク追加
    return 2;
}

//テーブルのセル数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return _objects.count;
    if( section == 0) {
        return taskList.count;
    }else{
        return 1;
    }
}


/**
 *  セルの高さ
 */
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35;
}

//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
	if( indexPath.section==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"taskListCell"];

        cell.textLabel.text = [taskList objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [timeList objectAtIndex:indexPath.row];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"addTaskCell"];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


/*
 * テーブルのセル選択時の処理
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if( indexPath.section == 0 ){
        selectedTaskIndex = indexPath.row;
        // ピッカービューを表示
        [self showPicker:indexPath.row];
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

//

/*******************
 * ピッカービュー
 *******************/

/*
 * 列数
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

/*
 * 行数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        // 一番左
        return 24;
    }else{
        // その次は5行あるとする。
        return 4;
    }
}

/*
 * タイトル
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        // 1番左の列の中身
        NSString *hourList[] = {@"0時間", @"1時間", @"2時間", @"3時間"
            ,@"4時間", @"5時間", @"6時間", @"7時間", @"8時間"
            ,@"9時間", @"10時間", @"11時間", @"12時間", @"13時間"
            ,@"14時間", @"15時間", @"16時間", @"17時間", @"18時間"
            ,@"19時間", @"20時間", @"21時間", @"22時間", @"23時間"};
        return hourList[row];
    }else{
        // 左から2番目の列の中身
        NSString *minuteList[] = {@"0分", @"15分", @"30分", @"45分" };
        return minuteList[row];
    }
}

//テキストフィールドを編集する直前に呼び出されます
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    return YES;  //これをNOにすると、キーボードが出ません
}
- (void)textFieldDidBeginEditing:(UITextField *)theTextField {
    if (theTextField == self.textField) {
        [textField resignFirstResponder];
        [self showPicker];
    }
}

/*
 * ピッカーを表示する
 */
- (void)showPicker:(NSInteger)index2 {
    pickerViewPopup = [[UIActionSheet alloc] initWithTitle:@"select time"
                                                  delegate:self
                                         cancelButtonTitle:nil
    destructiveButtonTitle:nil
    otherButtonTitles:nil];

    // Add the picker
    self.timePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,0,0)];
    
    self.timePickerView.delegate = self;
    self.timePickerView.showsSelectionIndicator = YES; // note this is default to NO
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]
    initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                  target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]
                                initWithTitle:@"完了"
                                style:UIBarButtonItemStyleDone
                                target:self action:@selector(closePicker:)];
    [barItems addObject:doneBtn];

    [pickerToolbar setItems:barItems animated:YES];
    
    [pickerViewPopup addSubview:pickerToolbar];
    [pickerViewPopup addSubview:self.timePickerView];
    [pickerViewPopup showInView:self.view];
    
    [pickerViewPopup setBounds:CGRectMake(0,0,320, 400)];
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
}

/*
 * ピッカービューの完了ボタン押下時の処理
 */
-(BOOL)closePicker:(id)index{
    
    //TODO 共通化
    NSString *hourList[] = {@"0時間", @"1時間", @"2時間", @"3時間"
        ,@"4時間", @"5時間", @"6時間", @"7時間", @"8時間"
        ,@"9時間", @"10時間", @"11時間", @"12時間", @"13時間"
        ,@"14時間", @"15時間", @"16時間", @"17時間", @"18時間"
        ,@"19時間", @"20時間", @"21時間", @"22時間", @"23時間"};
    NSString *minuteList[] = {@"0分", @"15分", @"30分", @"45分" };

    //デリゲートにタスクを格納
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];

    //工程情報の取得
    NippouPhaseInfoEntity *phaseInfo = appDelegate.todayInfo.phaseInfoList[self.phaseIndex];

    //リストから全件取得する処理が必要    
    NippouTaskInfoEntity *taskInfo = phaseInfo.taskInfoList[selectedTaskIndex];
    taskInfo.workTime = [NSString stringWithFormat:@"%@ %@", hourList[[self.timePickerView selectedRowInComponent:0]], minuteList[[self.timePickerView selectedRowInComponent:1]] ];

    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];

    //テーブルの更新
    timeList[selectedTaskIndex] = taskInfo.workTime;
    [self.taskTableView reloadData];

    return YES;
}


- (IBAction)moveToNippouMain:(id)sender {
    
    UIViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
    [self.navigationController popToViewController:parent animated:YES];

}
@end
