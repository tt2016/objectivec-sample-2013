//
//  NippouUtil.m
//  Nippou
//
//  Created by tt2016 on 2012/11/18.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouUtil.h"
#import "NippouPhaseInfoEntity.h"
#import "NippouTaskInfoEntity.h"

@implementation NippouUtil


/*
 * 時間の数値から文字列への変換
 */
+(NSString *)timeValueToString:(NSNumber *)timeVal {
    
    NSString *timeStr;

    //時間
    NSNumber *timeHour = [NSNumber numberWithInt:timeVal.intValue/60];

    //分
    NSNumber *timeMinute = [NSNumber numberWithInt:timeVal.intValue - (timeHour.intValue * 60)];
    
    if( timeMinute.intValue > 0 ){
        timeStr = [NSString stringWithFormat:@"%@%@%@%@", timeHour, @"時間", timeMinute, @"分"];
    }else{
        timeStr = [NSString stringWithFormat:@"%@%@", timeHour, @"時間"];
    }
    return timeStr;
}


/*
 * 時間の数値から文字列(時間)への変換
 */
+(NSString *)timeValueToStringHour:(NSNumber *)timeVal {
    
    NSString *timeStr;
    
    NSNumber *timeInt = [NSNumber numberWithInt:timeVal.intValue];
    timeStr = [NSString stringWithFormat:@"%@%@", timeInt, @"時間"];
    
    return timeStr;
}


/*
 * 時間の数値から文字列(分)への変換
 */
+(NSString *)timeValueToStringMinute:(NSNumber *)timeVal {
    
    NSString *timeStr;
    
    NSNumber *timeDouble = [NSNumber numberWithDouble:(timeVal.doubleValue - timeVal.intValue) * 60];
    
    if( timeDouble.doubleValue > 0 ){
        timeStr = [NSString stringWithFormat:@"%@%@", timeDouble, @"分"];
    }else{
        timeStr = @"00分";
    }
    return timeStr;
}


/*
 * 時間の文字列から数字への変換
 */
+(NSString *)timeStringToValue:(NSNumber *)timeVal {
    
    NSString *timeStr;
    
    NSNumber *timeInt = [NSNumber numberWithInt:timeVal.intValue];
    NSNumber *timeDouble = [NSNumber numberWithDouble:(timeVal.doubleValue - timeVal.intValue) * 60];
    
    if( timeDouble.doubleValue > 0 ){
        timeStr = [NSString stringWithFormat:@"%@%@%@%@", timeInt, @"時間", timeDouble, @"分"];
    }else{
        timeStr = [NSString stringWithFormat:@"%@%@", timeInt, @"時間"];
    }
    return timeStr;
}

/*
 * 時間の文字列から数字への変換
 */
+(NSNumber *)timeStringToValue:(NSString *)hour:(NSString *)minute {
    
    NSString *hourVal;
    NSString *minuteVal;
    
    hourVal = [hour stringByReplacingOccurrencesOfString:@"時間" withString:@""];
    minuteVal = [minute stringByReplacingOccurrencesOfString:@"分" withString:@""];
    
    NSNumber *timeNumber = [NSNumber numberWithInt: hourVal.intValue * 60 + minuteVal.intValue];
//    [NSNumber numberWithDouble:(hourVal.doubleValue + (minuteVal.doubleValue / 60)) ];

    return timeNumber;
}


/*
 * 時間の文字列から数字(時間)への変換
 */
+(NSString *)hourStringToValue:(NSNumber *)timeVal {
    
    NSString *timeStr;
    
    NSNumber *timeInt = [NSNumber numberWithInt:timeVal.intValue];
    NSNumber *timeDouble = [NSNumber numberWithDouble:(timeVal.doubleValue - timeVal.intValue) * 60];
    
    if( timeDouble.doubleValue > 0 ){
        timeStr = [NSString stringWithFormat:@"%@%@%@%@", timeInt, @"時間", timeDouble, @"分"];
    }else{
        timeStr = [NSString stringWithFormat:@"%@%@", timeInt, @"時間"];
    }
    return timeStr;
}


+(NSString *)timeSumFromPhaseInfo:(NippouPhaseInfoEntity *)phaseInfo {
    
    double timeSum = 0;
    
    for(NippouTaskInfoEntity *task in phaseInfo.taskInfoList){
        
        timeSum = timeSum + task.workTime.doubleValue;
    }
    
    return [NippouUtil timeValueToString:[NSNumber numberWithDouble:timeSum] ];
}

@end
