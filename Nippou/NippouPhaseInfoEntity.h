//
//  NippouPhaseInfoEntity.h
//  Nippou
//
//  Created by tt2016 on 2012/11/04.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NippouPhaseInfoEntity : NSObject{
    

    //工程名
    NSString *phaeseName;
    
    //タスクリスト
    NSMutableArray *taskInfoList;
}

@property (nonatomic,retain) NSString *phaseName;
@property (nonatomic,retain) NSMutableArray *taskInfoList;

@end
