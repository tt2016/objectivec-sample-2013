//
//  NippouMainViewController.m
//  Nippou
//
//  Created by tt2016 on 2012/09/15.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouMenuViewController.h"
#import "NippouMainViewController.h"
#import "NippouDailyInfoEntity.h"
#import "NippouTaskInfoEntity.h"
#import "NippouPhaseInfoEntity.h"

#import "NippouUtil.h"

#import "NippouAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface NippouMainViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation NippouMainViewController

@synthesize inputTextView;
@synthesize mainTable;
@synthesize pickerViewTime;
@synthesize pickerViewHours;
@synthesize workTimeNameList;
@synthesize workTimeMinuteList;
@synthesize workTimeHourList;
@synthesize phaseNameList;
@synthesize taskNameList;
@synthesize taskHourList;
@synthesize taskMinuteList;
@synthesize selectedItemIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.editing = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    /* 初期値 */
    workTimeNameList = [[NSMutableArray alloc] initWithObjects:@"始業", @"終業", @"休憩", @"稼働",@"時間外", nil];
    workTimeHourList = [[NSMutableArray alloc] initWithObjects:@"9時", @"17時", @"", @"7時間",@"", nil];
    workTimeMinuteList = [[NSMutableArray alloc] initWithObjects:@"00分", @"30分", @"45分", @"45分",@"0分", nil];

    //閉じるボタンの作成
	UIView* accessoryView =[[UIView alloc] initWithFrame:CGRectMake(0,0,320,45)];
	accessoryView.backgroundColor = [UIColor blackColor];

	UIButton* closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	closeButton.frame = CGRectMake(270,10,45,28);
	[closeButton setTitle:@" 完了" forState:UIControlStateNormal];
    
	// ボタンを押したときに呼ばれる動作を設定
	[closeButton addTarget:self action:@selector(closeKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    
	// ボタンをViewに追加
	[accessoryView addSubview:closeButton];
    
	// ビューをUITextFieldのinputAccessoryViewに設定
	inputTextView.inputAccessoryView = accessoryView;
    
     // キーボードが表示されたときのNotificationをうけとります。（後で）
    [self registerForKeyboardNotifications];
    
    //テキストビューのボーダー
    [[self.inputTextView layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.inputTextView layer] setBorderWidth:1];
    [[self.inputTextView layer] setCornerRadius:15];
    
    //デリゲートから情報を取得
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];

    //タスク情報の作成
    appDelegate.todayInfo = [[NippouDailyInfoEntity alloc] init];
    appDelegate.todayInfo.phaseInfoList = [[NSMutableArray alloc] init];

    NSDate *today = [NSDate date]; //現在時刻を取得
    
    NSString *dateStr;
    
    // NSDateFormatterを用意します。
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    // 変換用の書式を設定します。
    [formatter setDateFormat:@"YYYY/MM/dd"];
    
    // NSDate を NSString に変換します。
    dateStr = [formatter stringFromDate:today];
    
    self.title = dateStr;
    
	NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
    
    // 削除する
//     [nippouCoreData deleteStore];
    
    /* データを取得する (fetchObject:) */
    
    NSInteger maxCount = [nippouCoreData countObjects:@"TaskInfo"];
    
    for(int idx=0; idx< maxCount; idx++){
        
        NSManagedObject *managedObject = [nippouCoreData fetchObject:@"TaskInfo" WithRow:idx AndSection:0];

        NSLog(@"%@", [managedObject valueForKey:@"task_id"]);
        NSLog(@"%@", [managedObject valueForKey:@"task_name"]);
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    //画面表示用の作業実績・作業時間をセット
    self.phaseNameList = [[NSMutableArray alloc] init];
    self.taskNameList = [[NSMutableArray alloc] init];
    self.taskHourList = [[NSMutableArray alloc] init];
    self.taskMinuteList = [[NSMutableArray alloc] init];

    //デリゲートから日報情報を取得
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];

    NSString *phaseName;
    
    //画面表示用の作業実績・作業時間をセット
    for(NippouPhaseInfoEntity *phaseInfo in appDelegate.todayInfo.phaseInfoList){
        
        phaseName = phaseInfo.phaseName;
        for(NippouTaskInfoEntity *task in phaseInfo.taskInfoList){
            [self.phaseNameList addObject:phaseName];
            [self.taskNameList addObject:task.taskName];
            [self.taskHourList addObject:[NippouUtil timeValueToStringHour:task.workTime]];
            [self.taskMinuteList addObject:[NippouUtil timeValueToStringMinute:task.workTime]];
        }
    }
    
    
    self.taskTable.scrollEnabled =FALSE;
    [self.mainTable reloadData];
    [self.taskTable reloadData];
	[super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
    }
    [super viewWillDisappear:animated];
}





- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}


/*
 * キーボード表示時
 */
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    CGPoint scrollPoint = CGPointMake(0.0,320.0);
    [scrollView setContentOffset:scrollPoint animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}

/*
 * キーボードの閉じるボタン押下時
 */
-(void)closeKeyboard:(id)sender{
    [inputTextView resignFirstResponder];
}

- (void)viewDidUnload
{
    inputTextView = nil;
    [self setInputTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


/*******************
 * テーブルの情報
 *******************/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.selectedItemIndex = indexPath.row;

    if( [tableView isEqual:self.mainTable] ){
    
        if( indexPath.row <= 1) {
            //出勤・退勤
            pickerViewTime = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,0,0)];
                        
            //選択値（時間）を引き渡す
            [self showPicker:tableView:pickerViewTime:self.workTimeHourList[indexPath.row]:self.workTimeMinuteList[indexPath.row]];
        } else if( indexPath.row == 2) {
            //休憩
            pickerViewHours = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,0,0)];
            [self showPicker:tableView:pickerViewHours:self.workTimeHourList[indexPath.row]:self.workTimeMinuteList[indexPath.row]];
        }
    }else{
        //作業実績
//        pickerViewHours = [[UIPickerView alloc] initWithFrame:CGRectMake(0,44,0,0)];
//        [self showPicker:tableView:pickerViewHours:self.taskHourList[indexPath.row]:self.taskMinuteList[indexPath.row]];
    }
}

/**
 * セクション数
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //勤怠情報
    //作業実績
    //報告・連絡・相談
    return 1;
}

/**
 *  セクションごとのセルの数
 */
- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {

	if( [tableView isEqual:self.mainTable]){
        //作業時間
        return self.workTimeNameList.count;
    }else{
        //作業実績
        NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
        return [nippouCoreData countObjects:@"TaskInfo"];
    }
}

/**
 *  セルの高さ
 */
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

	switch(indexPath.section) {
		case 0:
			return 45;
		case 1:
			return 36;
		case 2:
			return 60;
		case 3:
			return 30;
		default:
			return 90;
	}
 }

/*
 * セクションヘッダー高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 30;
}

/*
 * セクションフッターの高さ
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 10;
}

/*
 セルの設定
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    /*
     
     作業実績（テーブル）
     報告・連絡・相談事項（テキスト）
     予定（テキスト）
     勤怠（ラベル）
     宛先（ラベル、アドレス帳から連絡先を取得する）
     
     */
    
    UITableViewCell *cell;
    if( [tableView isEqual:self.mainTable] ){

        //勤怠
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellKintai"];
                
        cell.textLabel.text = [workTimeNameList objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@", [workTimeHourList objectAtIndex:indexPath.row], [workTimeMinuteList objectAtIndex:indexPath.row]];
        
        if( indexPath.row >= 3 ){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = [UIColor colorWithRed:76/255.0 green:76/255.0 blue:76/255.0 alpha:1];
        }else{
            cell.textLabel.textColor = [UIColor colorWithRed:0/255.0 green:64/255.0 blue:128/255.0 alpha:1];
        }
    }else {
        //作業実績
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellSagyou"];

        NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
        NSManagedObject *managedObject = [nippouCoreData fetchObject:@"TaskInfo" WithRow:indexPath.row AndSection:0];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@%@%@%@", @"[", [managedObject valueForKey:@"phase_name"], @"]",[managedObject valueForKey:@"task_name"]];
        cell.detailTextLabel.text =[NippouUtil timeValueToString:[managedObject valueForKey:@"work_time"]];
 }

    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    return cell;
    
}

/*
 * ヘッダー
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionView = [[UIView alloc] init];
    
    UILabel *sectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 5.0f, 100.0f, 20.0f)];
    
    // 背景の色を変更
    sectionLabel.backgroundColor =
    [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1];
    
    sectionLabel.font = [UIFont systemFontOfSize:14];
    
    if( [tableView isEqual:self.mainTable] ){
        sectionLabel.text = @"勤怠情報";
    }else{
        sectionLabel.text = @"作業実績";
        //追加ボタン
        UIButton *insertButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        insertButton.frame = CGRectMake(240,0,70,30);
        [insertButton setTitle:@"追加" forState: UIControlStateNormal];
        [insertButton addTarget:self action:@selector(moveToPhase) forControlEvents:UIControlEventTouchUpInside];
        [sectionView addSubview:insertButton];

        //削除ボタン
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        deleteButton.frame = CGRectMake(160,0,70,30);
        [deleteButton setTitle:@"削除" forState: UIControlStateNormal];
        [deleteButton addTarget:self action:@selector(deleteTable) forControlEvents:UIControlEventTouchUpInside];
        [sectionView addSubview:deleteButton];
    }
    
    [sectionView addSubview:sectionLabel];
    
    return sectionView;
    
}


//セクション名を取得する
-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger) section {
	switch(section) {
		case 0:
			return @"勤怠情報";
		case 1:
			return @"作業実績";
		case 2:
			return @"報告・連絡・相談";
		case 3:
			return @"報告・連絡・相談";
		default:
			return @"";
	}
}


/*
 *　工程選択画面に遷移する
 */
-(void)moveToPhase{

    [self performSegueWithIdentifier:@"moveToPhase" sender:self];
    
}

/*
 *　削除ボタン押下時の処理
 */
-(void)deleteTable{

    //編集モード開始
    if( !self.editing ){
        self.taskTable.editing = TRUE;
        self.editing = TRUE;
    }else{
        self.taskTable.editing = FALSE;
        self.editing = FALSE;
    }
}

/*******************
 * ピッカービュー
 *******************/

/*
 * 列数
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}


/*
 * 行数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if([pickerViewTime isEqual: pickerView]){
        //作業時間
        if (component == 0) {
            return 24;
        }else {
            return 4;
        }
    }else if([pickerViewHours isEqual: pickerView]){
        //作業実績
        if (component == 0) {
            return 24;
        }else {
            return 4;
        }
    }else{
        return 0;
    }
}


/*
 * タイトル
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    if (component == 0) {
        // 1番左の列の中身
        if([pickerViewTime isEqual: pickerView]){
            //時間選択の場合

            NSString *time[] = {@"0時", @"1時", @"2時", @"3時"
                                ,@"4時", @"5時", @"6時", @"７時", @"8時"
                                ,@"9時", @"10時", @"11時", @"12時", @"13時"
                                ,@"14時", @"15時", @"16時", @"17時", @"18時"
                                ,@"19時", @"20時", @"21時", @"22時", @"23時"};
            return time[row];
        }else{
            NSString *hours[] = {@"0時間", @"1時間", @"2時間", @"3時間"
                                ,@"4時間", @"5時間", @"6時間", @"７時間", @"8時間"
                                ,@"9時間", @"10時間", @"11時間", @"12時間", @"13時間"
                                ,@"14時間", @"15時間", @"16時間", @"17時間", @"18時間"
                                ,@"19時間", @"20時間", @"21時間", @"22時間", @"23時間"};
            return hours[row];
        }
        
    }else {
        // 左から2番目の列の中身
        NSString *minutes[] = {@"00分", @"15分", @"30分", @"45分"};
        return minutes[row];
    }
}

/*
 * ピッカーを表示する
 */
- (void)showPicker:(UITableView *)tableView:(UIPickerView *)pickerView:(NSString *)hour:(NSString *)minute {
    
    pickerViewPopup = [[UIActionSheet alloc] initWithTitle:@"select"
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:nil];
    
    pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES; // note this is default to NO

    //ツールバーの設定
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                  target:self action:nil];
    [barItems addObject:flexSpace];
    
    UIBarButtonItem *doneBtn;
    if( [tableView isEqual:self.mainTable] ){
        doneBtn = [[UIBarButtonItem alloc]
                    initWithTitle:@"完了"
                    style:UIBarButtonItemStyleDone
                    target:self action:@selector(closePickerWorkInfo:)];
    }else{
        doneBtn = [[UIBarButtonItem alloc]
                   initWithTitle:@"完了"
                   style:UIBarButtonItemStyleDone
                   target:self action:@selector(closePickerTaskInfo:)];
    }
    
    [barItems addObject:doneBtn];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [pickerViewPopup addSubview:pickerToolbar];
    [pickerViewPopup addSubview:pickerView];
    [pickerViewPopup showInView:self.view];    
    [pickerViewPopup setBounds:CGRectMake(0,0,320, 400)];
    
    NSString *hourList[] = {@"0時", @"1時", @"2時", @"3時"
        ,@"4時", @"5時", @"6時", @"7時", @"8時"
        ,@"9時", @"10時", @"11時", @"12時", @"13時"
        ,@"14時", @"15時", @"16時", @"17時", @"18時"
        ,@"19時", @"20時", @"21時", @"22時", @"23時"};
    NSString *minuteList[] = {@"00分", @"15分", @"30分", @"45分" };

    //初期値の設定
    int hourRow;
    for(int idx=0; idx< 24; idx++){
        
        if([pickerViewTime isEqual: pickerView]){

            if([hour isEqualToString: hourList[idx]] ){
                hourRow = idx;
            }
        }else{
            if( [hour isEqualToString: [NSString stringWithFormat:@"%@%@", hourList[idx], @"間"]] ){
                hourRow = idx;
            }
        }
    }

    int minuteRow;
    for(int idx=0; idx< 4; idx++){
        
        if(minute == minuteList[idx]){
            minuteRow = idx;
        }
    }

    [pickerView selectRow:hourRow inComponent:0 animated:NO];
    [pickerView selectRow:minuteRow inComponent:1 animated:NO];
}


// ピッカービューの行選択時の処理
- (void) pickerView: (UIPickerView*)pView didSelectRow:(NSInteger) row  inComponent:(NSInteger)component {
    
    UITableViewCell *cell;
        //勤怠
    cell = [mainTable dequeueReusableCellWithIdentifier:@"CellKintai"];
    cell.detailTextLabel.text = [workTimeHourList objectAtIndex:0];

}


/*
 * ピッカービューの完了ボタン押下時の処理
 */
-(BOOL)closePickerWorkInfo:(id)index{
    
    //TODO 共通化
    NSString *hourList[] = {@"0時", @"1時", @"2時", @"3時"
        ,@"4時", @"5時", @"6時間", @"7時間", @"8時"
        ,@"9時", @"10時", @"11時", @"12時", @"13時"
        ,@"14時", @"15時", @"16時", @"17時", @"18時"
        ,@"19時", @"20時", @"21時", @"22時", @"23時"};
    NSString *minuteList[] = {@"00分", @"15分", @"30分", @"45分" };
    
    //デリゲートにタスクを格納
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //日報情報の取得
    NippouDailyInfoEntity *todayInfo = appDelegate.todayInfo;
    
    NSString *hour;
    NSString *minute;

    if( self.selectedItemIndex == 0) {
        //出勤時間
        hour = hourList[[self.pickerViewTime selectedRowInComponent:0]];
        minute = minuteList[[self.pickerViewTime selectedRowInComponent:1]];
    }else if( self.selectedItemIndex == 1) {
        //退勤時間
        hour = hourList[[self.pickerViewTime selectedRowInComponent:0]];
        minute = minuteList[[self.pickerViewTime selectedRowInComponent:1]];
    }else if( self.selectedItemIndex == 2) {
        //休憩時間
        hour = [NSString stringWithFormat:@"%@%@", hourList[[self.pickerViewHours selectedRowInComponent:0]], @"間"];
        minute = minuteList[[self.pickerViewHours selectedRowInComponent:1]];
    }else{
        //残業時間
    }
    
    //リストから全件取得する処理が必要
    //テーブルの更新
    workTimeHourList[self.selectedItemIndex] = hour;
    workTimeMinuteList[self.selectedItemIndex] = minute;
    
    [self.mainTable reloadData];
    [self.taskTable reloadData];
    
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
    
    return YES;
}


/*
 * ピッカービューの完了ボタン押下時の処理
 */
-(BOOL)closePickerTaskInfo:(id)index{
    
    //TODO 共通化
    NSString *hourList[] = {@"0時", @"1時", @"2時", @"3時"
        ,@"4時", @"5時", @"6時間", @"7時間", @"8時"
        ,@"9時", @"10時", @"11時", @"12時", @"13時"
        ,@"14時", @"15時", @"16時", @"17時", @"18時"
        ,@"19時", @"20時", @"21時", @"22時", @"23時"};
    NSString *minuteList[] = {@"00分", @"15分", @"30分", @"45分" };
    
    NSString *hour;
    NSString *minute;
    
    //テーブルの更新
    hour = [NSString stringWithFormat:@"%@%@", hourList[[self.pickerViewHours selectedRowInComponent:0]], @"間"];
    minute = minuteList[[self.pickerViewHours selectedRowInComponent:1]];
    taskHourList[self.selectedItemIndex] = hour;
    taskMinuteList[self.selectedItemIndex] = minute;
    
    [self.mainTable reloadData];
    [self.taskTable reloadData];
    
    //デリゲートメモリ情報の更新
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];

    NSString *phaseName = self.phaseNameList[self.selectedItemIndex];
    NSString *taskName = self.taskNameList[self.selectedItemIndex];
    
    for(NippouPhaseInfoEntity *phaseInfo in appDelegate.todayInfo.phaseInfoList){
        
        if( [phaseName isEqual:phaseInfo.phaseName] ){
            
            for(NippouTaskInfoEntity *taskInfo in phaseInfo.taskInfoList){
                
                if( [taskName isEqual:taskInfo.taskName] ){
                
                    taskInfo.workTime = [NippouUtil timeStringToValue:hour :minute];
                    break;
                }
            }
        }
    }
    
    [pickerViewPopup dismissWithClickedButtonIndex:0 animated:YES];
    
    return YES;
}


-(void)changeTime:(id)sender{
//    return YES;
}


/*
 * テーブル編集処理
 */
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /* データを削除する (deleteObject:) */
        
        // rowとsectionを指定して削除する
        NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
        [nippouCoreData deleteObject:@"TaskInfo" WithRow:indexPath.row AndSection:0];
        [tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


//アラートの表示
- (void)showAlert:(NSString*)title text:(NSString*)text {
    UIAlertView* alert=[[UIAlertView alloc]
                         initWithTitle:title message:text delegate:nil
                         cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

//メール送信
- (IBAction)sendMail:(id)sender {
    
    //メール送信可能かどうかのチェック(1)
    if (![MFMailComposeViewController canSendMail]) {
        [self showAlert:@"" text:@"メール送信できません"];
        return;
    }
    
    //メールコントローラの生成(2)
    MFMailComposeViewController* pickerCtl=
    [[MFMailComposeViewController alloc] init];
    pickerCtl.mailComposeDelegate=self;
    
    NSString *header =@"宛先： 松井MGR"
                    @"\n 写し： 内野CEO、新井COO"
                    @"\n 発信： 0066 常包健史"
                    @"\n SE2東1"
                    @"\n MIND相互半蔵門ビル3F";

    NSString *workInfo =  [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@"
                           , @"\n     1.勤怠情報"
                    @"\n        日付 ："
                           ,self.title
                    ,@"\n        [時刻]"
                    @"\n        出勤 ："
                           ,self.workTimeHourList[0],self.workTimeMinuteList[0]
                    ,@"\n        始業 ："
                           ,self.workTimeHourList[0],self.workTimeMinuteList[0]
                    ,@"\n        終業 ："
                           ,self.workTimeHourList[1],self.workTimeMinuteList[1]
                    ,@"\n        退勤 ："
                           ,self.workTimeHourList[1],self.workTimeMinuteList[1]];
    
//    始業〜終業合計 ：10:45
//    うち作業時間 ：9:15（うち時間外 ： 1:30)
//    うち休憩時間 ：1:30

    NSString *memo = [NSString stringWithFormat:@"%@%@%@"
                      ,@"\n        3.報告・連絡・相談事項"
                      @"\n        特になし。"
                      ,@"\n        4.予定情報(打合せ、出張、研修、イベントなど)"
                      ,@"\n            　9/9（日）　休日出勤（DR東西切り替え試験リハーサル）"];

    NSString *tempTask = @"";

    //デリゲートから日報情報を取得
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //画面表示用の作業実績・作業時間をセット
    for(NippouPhaseInfoEntity *phaseInfo in appDelegate.todayInfo.phaseInfoList){
        
        //工程出力
        tempTask = [NSString stringWithFormat:@"%@%@%@", tempTask, phaseInfo.phaseName, @"\n"];
        
        for(NippouTaskInfoEntity *taskInfo in phaseInfo.taskInfoList){
            
            //タスク名・時間
            tempTask = [NSString stringWithFormat:@"%@%@%@%@%@%@",tempTask, @"　　　", taskInfo.taskName, @"："
                        ,[NippouUtil timeValueToString:taskInfo.workTime], @"\n"];
            
        }
    }
    
    NSString *taskInfo = [NSString stringWithFormat:@"%@%@"
                          ,@"\n        5.参画案件一覧(参画期間)"
                          @"\n        5.1.T-H-1100005 営シ１部派遣（常包）(2011/04/01 〜 2012/03/31)"
                          @"\n        6.案件毎 作業実績"
                          @"\n        6.1.T-H-1100005 営シ１部派遣（常包）\n"
                          ,tempTask ];
    

    
/*                          @"\n            (1) 受注前活動
                          ： 00:00
                         @"\n                (2) 要件定義       ： 01:00
	[IFRS]調査依頼対応	1h
                         @"\n                (3) 基本設計       ： 00:00
    　(4) 詳細設計       ： 00:00
    (5) 製造           ： 00:00
    (6) 単体テスト     ： 00:00
    (7) システムテスト ： 00:00
    (8) 稼動準備       ： 00:00
    (9) 保守           ： 08:15
	[保守]障害調査・対策検討	2h
	[保守]データ補正フォロー	1h
	[保守]定常作業	0.75h
	[保守]財務統制依頼対応	2h
	[保守]進捗宿題対応	0.5h
	[保守]問合せ対応	1h
	[その他]課MTG	1h
    
    */
    
    NSString *mailContext = [NSString stringWithFormat:@"%@%@%@%@"
                             ,header,workInfo,memo,taskInfo];

    
    //メールのテキストの指定(3)
    [pickerCtl setMessageBody:mailContext isHTML:NO];

    NSArray *toRecipients = [NSArray arrayWithObject:@"kouji.yamamoto@example.com"];
//    NSArray *toRecipients = [NSArray arrayWithObject:@"kazuya.kondo@gmail.com"];
//    NSArray *ccRecipients = [NSArray arrayWithObjects:@"yuesugi.18198107@gmail.com", @"kazuhi.fujimura@gmail.com", nil];

    //宛先の設定
    [pickerCtl setToRecipients:toRecipients];
//    [pickerCtl setCcRecipients:ccRecipients];

    //メールコントローラのビューを開く(5)
    [self presentModalViewController:pickerCtl animated:YES];
}

//HTTP送信を行う
- (IBAction)sendHttpRequest:(id)sender {

    NSMutableURLRequest *request=[NSMutableURLRequest
//                                  requestWithURL:[NSURL URLWithString:@"http://localhost:3000/tasks"]
                                  requestWithURL:[NSURL URLWithString:@"http://ec2-54-248-237-231.ap-northeast-1.compute.amazonaws.com:3001/tasks"]
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:30.0];

    //CoreDataから情報取得
    NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
    NSManagedObject *managedObject = [nippouCoreData fetchObject:@"TaskInfo" WithRow:1 AndSection:0];

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"3001" forKey:@"task[id]"];
    [params setObject:[managedObject valueForKey:@"phase_name"] forKey:@"task[phaseName]"];
    [params setObject:[managedObject valueForKey:@"task_name"] forKey:@"task[taskName]"];

/*
    [params setObject:[managedObject valueForKey:@"emp_id"] forKey:@"emp_id"];
    [params setObject:[managedObject valueForKey:@"phase_id"] forKey:@"phase_id"];
    [params setObject:[managedObject valueForKey:@"phase_name"] forKey:@"phase_name"];
    [params setObject:[managedObject valueForKey:@"project_id"] forKey:@"project_id"];
    [params setObject:[managedObject valueForKey:@"project_name"] forKey:@"project_name"];
    [params setObject:[managedObject valueForKey:@"project_task_id"] forKey:@"project_task_id"];
    [params setObject:[managedObject valueForKey:@"task_id"] forKey:@"task_id"];
    [params setObject:[managedObject valueForKey:@"task_name"] forKey:@"task_name"];
    [params setObject:[managedObject valueForKey:@"work_date"] forKey:@"work_date"];
    [params setObject:[managedObject valueForKey:@"work_time"] forKey:@"work_time"];
*/
    
    NSString *body = Nil;
    // NSDictionaryをループする
    NSEnumerator *keyEnumerator = [params keyEnumerator];
    id key;
    while (key = [keyEnumerator nextObject]) {

        if( body == Nil ){
            body = [NSString stringWithFormat:@"%@=%@", key, [params objectForKey:key]];
        }else{
            body = [body stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", key, [params objectForKey:key]]];
        }
    }
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    // 送信
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    // HTTPBodyには、NSData型で設定する
//    request.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
}


@end
