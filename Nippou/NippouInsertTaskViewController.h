//
//  NippouInsertTaskViewController.h
//  Nippou6
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouTaskInfoEntity.h"
#import "NippouPhaseInfoEntity.h"
#import "NippouManageCoreData.h"
#import "NippouUtil.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface NippouInsertTaskViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource, NSFetchedResultsControllerDelegate>{
    
    //作業一覧
    NSArray *taskList;
    
    IBOutlet UIPickerView *timePickerView;
    
    //入力フィールド
    IBOutlet UITextField *inputText;
        
    //作業時間
    NSMutableArray *timeList;

    //工程
    int phaseIndex;
    NSString *phaseName;
    
    NSManagedObjectContext *context;
}


@property (nonatomic, retain) UITextField *inputText;
@property (nonatomic, retain) UIPickerView *timePickerView;
@property int phaseIndex;
@property (copy, nonatomic) NSString *phaseName;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
