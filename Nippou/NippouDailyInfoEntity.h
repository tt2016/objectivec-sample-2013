//
//  NippouDailyInfoEntity.h
//  Nippou
//
//  Created by tt2016 on 2012/10/29.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NippouDailyInfoEntity : NSObject{
    
    //勤怠情報
    //出勤時間
    NSString *startTime;
    
    //退勤時間
    NSString *endTime;
    
    //休憩時間
    NSString *restTime;
    
    //稼働時間
    NSString *workTime;
    
    //残業時間
    NSString *overTime;
    
    //工程別の作業実績
    NSMutableArray *phaseInfoList;

}

@property (nonatomic,retain) NSMutableArray *phaseInfoList;

@property (nonatomic,retain) NSString *startTime, *endTime, *restTime, *workTime, *overTime;

@end
