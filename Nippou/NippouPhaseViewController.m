//
//  NippouPhaseViewController.m
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouPhaseViewController.h"
#import "NippouDetailViewController.h"
#import "NippouTaskInfoEntity.h"
#import "NippouPhaseInfoEntity.h"
#import "NippouUtil.h"

#import <QuartzCore/QuartzCore.h>

@interface NippouPhaseViewController () {
    NSMutableArray *_objects;
}
@end

@implementation NippouPhaseViewController

@synthesize phaseIndex;

- (void)awakeFromNib
{

    [super awakeFromNib];
}

- (void)viewDidLoad
{
        
    phaseNameList = [[NSMutableArray alloc] initWithObjects:@"受注前活動",@"要件定義", @"基本設計",@"詳細設計",@"製造",@"単体テスト",@"システムテスト",@"稼働準備",@"保守", nil];

    //下段のビューのグラデーション設定
    CAGradientLayer *pageGradient = [CAGradientLayer layer];
    pageGradient.frame = bottomView.bounds;
    pageGradient.colors =
    [NSArray arrayWithObjects:
     (id)[UIColor colorWithRed:133/255.0 green:58/255.0 blue:61/255.0 alpha:1].CGColor,
     (id)[UIColor colorWithRed:102/255.0 green:0/255.0 blue:0/255.0 alpha:1].CGColor,nil];
    [bottomView.layer insertSublayer:pageGradient atIndex:0];
    
    [self.view addSubview:bottomView];
    
    /* 編集終了ボタン */
    //ボタンの背景色変更
    [[sendButton layer] setCornerRadius:8.0f];
    [[sendButton layer] setMasksToBounds:YES];
    [[sendButton layer] setBorderWidth:1.0];
    [[sendButton layer] setBackgroundColor:
     [[UIColor colorWithRed:80/255.0 green:0/255.0 blue:34/255.0 alpha:1]CGColor]];
    [sendButton.layer setBorderColor:
     [[UIColor colorWithRed:102/255.0 green:0/255.0 blue:34/255.0 alpha:1]CGColor]];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    //影
    sendButton.layer.shadowOffset = CGSizeMake(2, 2);
    sendButton.layer.shadowOpacity = 0.5f;
    sendButton.layer.shadowColor = [UIColor blackColor].CGColor;
    //グラデーション
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    gradient2.frame = sendButton.bounds;
    gradient2.colors = [NSArray arrayWithObjects:
                        (id)[UIColor colorWithRed:166/255.0 green:101/255.0 blue:121/255.0 alpha:1].CGColor
                        , (id)[UIColor colorWithRed:56/255.0 green:0/255.0 blue:30/255.0 alpha:1].CGColor
                        , nil];
    [sendButton.layer insertSublayer:gradient2 atIndex:0];

    [super viewDidLoad];
    
}

/*
 * 画面表示時の処理
 */
- (void)viewWillAppear:(BOOL)animated {
    
    //デリゲートから日報情報を取得
    NippouAppDelegate *appDelegate = (NippouAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //リストから全件取得する処理が必要
/*    phaseNameList = [[NSMutableArray alloc] init];
    phaseTimeList = [[NSMutableArray alloc] init];
    
    for(NippouPhaseInfoEntity *phase in appDelegate.todayInfo.phaseInfoList){
        
        [phaseNameList addObject:phase.phaseName];
        
        [phaseTimeList addObject:[NippouUtil timeSumFromPhaseInfo:phase]];
    }
*/
    //セルの選択解除
    [phaseTable deselectRowAtIndexPath:[phaseTable indexPathForSelectedRow] animated:NO];
	    
    [phaseTable reloadData];
    [super viewWillAppear:animated];
}


/*
 * タスク編集画面への遷移時
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    UITableViewCell *cell = (UITableViewCell *)sender;
    int row = [phaseTable indexPathForCell:cell].row;
    if ( [[segue identifier] isEqualToString:@"gotoDetail"] ) {
        NippouDetailViewController *detailViewController = [segue destinationViewController];

        //選択された工程を次画面に渡す
        detailViewController.phaseName = phaseNameList[row];;
        detailViewController.phaseIndex = row;
    }
}
 

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return phaseNameList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"CustomCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.textLabel.text = [phaseNameList objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [phaseTimeList objectAtIndex:indexPath.row];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

@end
