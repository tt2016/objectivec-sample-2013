//
//  NippouUtil.h
//  Nippou
//
//  Created by tt2016 on 2012/11/18.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NippouPhaseInfoEntity.h"

@interface NippouUtil : NSObject

+(NSString *)timeValueToString:(NSNumber *)timeVal;
+(NSString *)timeValueToStringHour:(NSNumber *)timeVal;
+(NSString *)timeValueToStringMinute:(NSNumber *)timeVal;
+(NSNumber *)timeStringToValue:(NSString *)hour:(NSString *)minute;
+(NSString *)timeSumFromPhaseInfo:(NippouPhaseInfoEntity *)phaseInfo;

@end
