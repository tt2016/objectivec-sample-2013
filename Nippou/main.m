//
//  main.m
//  Nippou
//
//  Created by tt2016 on 2012/10/27.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NippouAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NippouAppDelegate class]));
    }
}
