//
//  NippouMainViewController.h
//
//  Created by tt2016 on 2012/09/15.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <CoreData/CoreData.h>

@interface NippouMainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate, NSFetchedResultsControllerDelegate>{

    IBOutlet UITableView *mainTable;
    IBOutlet UITableView *taskTable;

    //勤怠
    NSMutableArray *workTimeNameList;
    NSMutableArray *workTimeHourList;
    NSMutableArray *workTimeMinuteList;

    //作業一覧
    NSMutableArray *phaseNameList;
    NSMutableArray *taskNameList;
    NSMutableArray *taskHourList;
    NSMutableArray *taskMinuteList;

    //ピッカービュー
    UIActionSheet *pickerViewPopup;
    UIPickerView *pickerViewTime;
    UIToolbar *pickerToolbar;
    
    UIPickerView *pickerViewHours;
    
    IBOutlet UITextView *inputTextView;
    
    IBOutlet UIScrollView *scrollView;    
    
    //選択した項目の順番
    int selectedItemIndex;

    BOOL editing;

}

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) UITextView *inputTextView;
@property (strong, nonatomic) UITableView *mainTable, *taskTable;
@property (strong, nonatomic) UIPickerView *pickerViewTime;
@property (strong, nonatomic) UIPickerView *pickerViewHours;
@property int selectedItemIndex;
@property (strong, nonatomic) NSMutableArray *workTimeNameList, *workTimeHourList, *workTimeMinuteList, *phaseNameList, *taskNameList, *taskHourList, *taskMinuteList;

-(BOOL)closePicker:(id)index:(NSInteger)idx;

@end
