//
//  NippouMasterViewController.h
//  Nippou6
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NippouDetailViewController.h"

@interface NippouPhaseViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{

    IBOutlet UIButton *sendButton;
    NSMutableArray *phaseNameList;
    NSMutableArray *phaseTimeList;
    IBOutlet UITableView *phaseTable;
    IBOutlet UIView *bottomView;
    
    int phaseIndex;
}

@property (strong, nonatomic) NippouDetailViewController *detailViewController;
@property int phaseIndex;

@end
