//
//  NippouInsertTaskViewController.m
//  Nippou6
//
//  Created by tt2016 on 2012/09/02.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouInsertTaskViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface NippouInsertTaskViewController ()

@end

@implementation NippouInsertTaskViewController

@synthesize inputText;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/*
 * 決定ボタン押下時の処理
 */
- (IBAction)submitTask:(id)sender {

    //TODO 共通化
    NSString *hourList[] = {@"0時間", @"1時間", @"2時間", @"3時間"
        ,@"4時間", @"5時間", @"6時間", @"7時間", @"8時間"
        ,@"9時間", @"10時間", @"11時間", @"12時間", @"13時間"
        ,@"14時間", @"15時間", @"16時間", @"17時間", @"18時間"
        ,@"19時間", @"20時間", @"21時間", @"22時間", @"23時間"};
    NSString *minuteList[] = {@"00分", @"15分", @"30分", @"45分" };
    
    NSNumber *workTime = [NippouUtil timeStringToValue:hourList[[self.timePickerView selectedRowInComponent:0]]:minuteList[[self.timePickerView selectedRowInComponent:1]] ];
    
    NippouCoreData* nippouCoreData = [NippouCoreData sharedInstance];
    
    /* CoreDataにデータを作成する */
    NSManagedObject *newManagedObject = [nippouCoreData newManagedObject:@"TaskInfo"];
    [newManagedObject setValue:self.phaseName forKey:@"phase_name"];           //工程名
    [newManagedObject setValue:self.inputText.text forKey:@"task_name"];    //作業名
    [newManagedObject setValue:workTime forKey:@"work_time"]; //作業時間
    [newManagedObject setValue:[NSDate date] forKey:@"time_stamp"];

    [nippouCoreData saveContext];
        
    //前画面に戻る
    UIViewController *parent = [self.navigationController.viewControllers objectAtIndex:3];
    [self.navigationController popToViewController:parent animated:YES];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TaskInfo" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"taskId" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.timePickerView.delegate = self;
    inputText.returnKeyType = UIReturnKeyDone;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/*******************
 * ピッカービュー
 *******************/

/*
 * 列数
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

/*
 * 行数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 24;
    }else{
        return 4;
    }
}

/*
 * タイトル
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        // 1番左の列の中身
        NSString *hourList[] = {@"0時間", @"1時間", @"2時間", @"3時間"
            ,@"4時間", @"5時間", @"6時間", @"7時間", @"8時間"
            ,@"9時間", @"10時間", @"11時間", @"12時間", @"13時間"
            ,@"14時間", @"15時間", @"16時間", @"17時間", @"18時間"
            ,@"19時間", @"20時間", @"21時間", @"22時間", @"23時間"};
        return hourList[row];
    }else{
        // 左から2番目の列の中身
        NSString *minuteList[] = {@"0分", @"15分", @"30分", @"45分" };
        return minuteList[row];
    }
}






@end
