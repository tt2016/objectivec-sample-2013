//
//  NippouTextViewController.m
//  Nippou6
//
//  Created by tt2016 on 2012/09/08.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import "NippouTextViewController.h"

@interface NippouTextViewController ()

@end

@implementation NippouTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


/*******************
 * ピッカービュー
 *******************/

/*
 * 列数
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

/*
 * 行数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        // 一番左
        return 4;
    }else{
        // その次は5行あるとする。
        return 4;
    }
}

/*
 * タイトル
 */
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        // 1番左の列の中身
        NSString *album[] = {@" 0時間", @" 1時間", @" 2時間", @" 3時間"};
        return album[row];
    }else{
        // 左から2番目の列の中身
        NSString *title[] = {@"00分", @"15分", @"30分", @"45分"};
        return title[row];
    }
}


@end
