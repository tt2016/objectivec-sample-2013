//
//  NippouManageCoreData.m
//  Nippou
//
//  Created by tt2016 on 2013/01/31.
//  Copyright (c) 2013年 tt2016. All rights reserved.
//

#import "NippouManageCoreData.h"

@implementation NippouManageCoreData

#pragma mark -
#pragma mark entity access method
- (NSMutableArray*)fetch:(NSString*)entityName limit:(int)limit {
	NSManagedObjectContext *context = self.managedObjectContext;
    
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
	[request setEntity:entity];
    
	NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
	[request setSortDescriptors:[NSArray arrayWithObject:sort]];
	[request setFetchLimit:limit];
    
	NSError *error = nil;
	NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		// Handle the error.
//		LOG(@"fetch error.");
	}
	return mutableFetchResults;
}

- (NSMutableArray*)all:(NSString*)entityName {
	[self fetch:entityName limit:0];
}

- (NSManagedObject*)entityForInsert:(NSString*)entityname {
	NSManagedObjectContext *context = self.managedObjectContext;
	return [NSEntityDescription insertNewObjectForEntityForName:entityname inManagedObjectContext:context];
}

@end
