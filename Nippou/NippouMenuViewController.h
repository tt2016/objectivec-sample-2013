//
//  NippouMenuViewController.h
//  Nippou
//
//  Created by tt2016 on 2012/10/06.
//  Copyright (c) 2012年 tt2016. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NippouMenuViewController : UIViewController{
    NSTimer *timer;
}
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *btnBackLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

- (void)driveClock:(NSTimer *)timer;
@end
